#opFileManagePlugin

ファイルを共有する

##install
see <https://github.com/amashigeseiji/opFileManagePlugin/wiki/How-to-install>

##機能
* コミュニティフォルダ/コミュニティ内でのファイル共有  
* 公開フォルダ/SNS内でのファイル共有  
* プライベートフォルダ/個人用のデータ  
※アップロードするファイルの種類に制限はありません。  

##使い方
* コミュニティ共有を利用する場合は、管理画面のプラグイン設定画面からopFileManagePluginの設定画面を表示し、「コミュニティ共有を使用する」を「使用する」にして保存してください。  
* プライベートフォルダを利用する場合は、管理画面のプラグイン設定画面からopFileManagePluginの設定画面を表示し、「プライベートフォルダを使用する」を「使用する」にして保存してください。  
* ファイルをアップロードするためには、フォルダが作成されている必要があります。「フォルダを作成する」リンクからフォルダを作成してください。  

##注意
* opUploadFilePlugin と同時に利用することはできません。opUploadFilePlugin がすでにインストールされている環境でこのプラグインを利用するときは、管理画面から opUploadFilePlugin を無効化するかアンインストールしてください。  
また、opUploadFilePlugin とは互換性がありませんので、opUploadFilePlugin でアップロードしたファイルは opFileManagePlugin から参照できず、逆に opFileManagePlugin からアップロードしたファイルを opUploadFilePlugin から参照することはできません。  
